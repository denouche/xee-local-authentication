const express = require('express')
const app = express()
const https = require('https')

// Go on dev.xee.com to create an application and get clientId and secretId to replace here
// Also add http://localhost:3000 to your authorized callback URI on dev.xee.com
const CLIENT_ID = "XXXXXXXX",
    SECRET_ID = "YYYYYYYY";

app.get('/', function (req, res) {
    let code = req.query.code
    let postData = `grant_type=authorization_code&code=${code}`
    let options = {
        hostname: 'cloud.xee.com',
        port: 443,
        path: '/v1/auth/access_token',
        method: 'POST',
        headers: {
            'Authorization': 'Basic ' + new Buffer(CLIENT_ID + ':' + SECRET_ID).toString('base64'),
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postData)
        }
    }
    const r = https.request(options, (rs) => {
        rs.on('data', (d) => {
            console.log('result: ' + d);
            res.set('Content-Type', 'text/plain');
            res.send(d)
        });
    });
    r.on('error', (e) => {
        console.error(e);
    });
    r.write(postData);
    r.end();
})

app.listen(3000, function () {
      console.log('Listening on port 3000')
})
